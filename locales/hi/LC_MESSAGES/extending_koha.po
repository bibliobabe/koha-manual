# Compendium of hi.
msgid ""
msgstr ""
"Project-Id-Version: compendium-hi\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-05-15 19:31-0300\n"
"PO-Revision-Date: 2018-05-15 19:51-0300\n"
"Language-Team: Koha Translation Team \n"
"Language: hi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../source/extending_koha.rst:4
msgid "Extending Koha"
msgstr "विस्तारित कोहा"

#: ../../source/extending_koha.rst:6
msgid ""
"This chapter shows how you can add various enhancements and customizations "
"in and around Koha by using mostly the existing configuration options."
msgstr ""

#: ../../source/extending_koha.rst:12
msgid "Amazon lookup script for Koha libraries"
msgstr "कोहा पुस्तकालयों के लिए अमेज़ॅन लुकअप स्क्रिप्ट"

#: ../../source/extending_koha.rst:14
msgid ""
"We order most of our materials from Amazon, so I've been looking for a "
"convenient way to tell if a book under consideration is in our catalog "
"already."
msgstr ""
"हम अमेज़न से हमारे माल की सबसे आदेश है, तो मैं बताने के लिए देख रहा हूँ एक सुविधाजनक तरीका "
"है अगर विचाराधीन एक किताब हमारी सूची में पहले से ही है।"

#: ../../source/extending_koha.rst:18
msgid "Greasemonkey & a custom user script fit the bill nicely:"
msgstr "Greasemonkey और एक कस्टम उपयोगकर्ता स्क्रिप्ट बिल बिलकुल फिट बैठता है:"

#: ../../source/extending_koha.rst:20
msgid "https://addons.mozilla.org/en-US/firefox/addon/748"
msgstr "https://addons.mozilla.org/en-US/firefox/addon/748"

#: ../../source/extending_koha.rst:22
msgid "http://userscripts.org/scripts/show/56847"
msgstr "http://userscripts.org/scripts/show/56847"

#: ../../source/extending_koha.rst:24
msgid "A few caveats:"
msgstr "कुछ निरंतर:"

#: ../../source/extending_koha.rst:26
msgid ""
"Like most scripts, this one was designed to work with Firefox; I haven't "
"explored getting it to work with other browsers."
msgstr ""
"सबसे लिपियों की तरह, यह एक फ़ायरफ़ॉक्स के साथ काम करने के लिए डिजाइन किया गया था; मैं "
"इसे अन्य ब्राउज़रों के साथ काम करने के लिए हो रही खोज नहीं की है।"

#: ../../source/extending_koha.rst:29
msgid ""
"I'm not a JavaScript programmer -- this was adapted from others' work. Just "
"a few lines would have to be changed to get the script to work with your "
"catalog."
msgstr ""
"मैं एक जावास्क्रिप्ट प्रोग्रामर नहीं हूँ - यह अन्यों' के काम से अनुकूलित किया गया था। बस कुछ "
"ही लाइनों अपनी सूची के साथ काम करने के लिए स्क्रिप्ट को पाने के लिए परिवर्तित किया जा "
"करने के लिए होता है।"

#: ../../source/extending_koha.rst:33
msgid ""
"It depends on the existence of ISBN for the item in question, so movies, "
"older books, etc. would not work."
msgstr ""
"यह प्रश्न में आइटम के लिए आई के अस्तित्व पर निर्भर करता है, तो फिल्में, पुराने किताबें, आदि "
"काम नहीं करेगा।"

#: ../../source/extending_koha.rst:36
msgid ""
"Others have added all sorts of bells & whistles: XISBN lookups to search for "
"related titles, custom messages based on the status of items (on order, on "
"hold, etc.), ... just search the UserScripts site for Amazon + library. For "
"a later date!"
msgstr ""
"अन्य ने सभी प्रकार की घंटियां और सीटी जोड़ दी हैं: एक्सआईएसबीएन वस्तुओं से संबंधित स्थिति "
"(ऑर्डर, ऑन होल्ड इत्यादि) के आधार पर संबंधित शीर्षक, कस्टम संदेश खोजने के लिए खोजता "
"है ... बस अमेज़ॅन + लाइब्रेरी के लिए उपयोगकर्ता मार्गदर्शिका साइट खोजें। बाद की तारीख के "
"लिए!"

#: ../../source/extending_koha.rst:44
msgid "Keyword Clouds"
msgstr "कीवर्ड क्लाउड"

#: ../../source/extending_koha.rst:46
msgid ""
"In addition to the traditional tag cloud available in Koha, there is a way "
"to generate clouds for popular subjects within Koha."
msgstr ""
"कोहा में उपलब्ध पारंपरिक टैग बादल के अलावा, वहाँ एक रास्ता कोहा भीतर लोकप्रिय विषयों के "
"लिए बादलों उत्पन्न करने के लिए है।"

#: ../../source/extending_koha.rst:49
msgid ""
"The :ref:`Author/Subject Cloud cron job <subject/author-clouds-label>` is "
"used to help with this process. This cron job sends its output to files."
msgstr ""
"यह :ref:`लेखक/विषय क्लाउड क्रॉन जॉब <subject/author-clouds-label>` इस प्रक्रिया में "
"मदद के लिए प्रयोग किया जाता है। यह क्रॉन जॉब फाइलों में अपना आउटपुट भेजता है।"

#: ../../source/extending_koha.rst:52
msgid "/home/koha/mylibrary/koharoot/koha-tmpl/cloud-author.html"
msgstr "/home/koha/mylibrary/koharoot/koha-tmpl/cloud-author.html"

#: ../../source/extending_koha.rst:54
msgid "/home/koha/yourlibrary/koharoot/koha-tmpl/cloud-subject.html"
msgstr "/home/koha/yourlibrary/koharoot/koha-tmpl/cloud-subject.html"

#: ../../source/extending_koha.rst:56
msgid ""
"This means that you can produce clouds for authors, collective author, all "
"kind of subjects, classifications, etc. And since it works on zebra indexes, "
"it is quick, even on large DBs. Tags clouds are sent to files. It's up to "
"library webmaster to deal with those files in order to include them in :ref:"
"`OPACMainUserBlock <opacmainuserblock-label>`, or include them into their "
"library CMS."
msgstr ""
"इसका मतलब है कि आप लेखकों, सामूहिक लेखक, सभी प्रकार के विषयों, वर्गीकरण इत्यादि के लिए "
"बादलों का उत्पादन कर सकते हैं और चूंकि यह ज़ेबरा इंडेक्स पर काम करता है, यह भी बड़े डीबी पर "
"भी तेज़ है। टैग बादलों को फाइलों को भेजा जाता है। यह उन फ़ाइलों से निपटने के लिए लाइब्रेरी "
"वेबमास्टर पर निर्भर है ताकि उन्हें इन्हें शामिल किया जा सके :ref:`OPACMainUserBlock "
"<opacmainuserblock-label>`, या उन्हें अपनी लाइब्रेरी सीएमएस में शामिल करें।"

#: ../../source/extending_koha.rst:63
msgid ""
"Some libraries even send the file into a Samba shared folder where webmaster "
"take them, eventually clean them a little bit before integrating them into "
"navigation widgets or pages."
msgstr ""
"कुछ पुस्तकालयों भी एक सांबा साझा फ़ोल्डर जहां वेबमास्टर उन्हें ले, अंततः उन्हें उन्हें नेविगेशन "
"विगेट्स या पृष्ठों में एकीकृत पहले एक छोटा सा साफ में फाइल भेज सकते हैं।"

#: ../../source/extending_koha.rst:70
msgid "Newest Titles Pulldown"
msgstr "नवीनतम टाइटल पुल्डडाउन"

#: ../../source/extending_koha.rst:72
msgid ""
"Often we want to add a way for our patrons to do searches for the newest "
"items. In this example I'll show you how to create a pull down menu of the "
"newest items by item type. These tips will work (with a couple changes) for "
"collection codes or shelving locations as well."
msgstr ""
"अक्सर हम हमारे संरक्षक नए आइटम के लिए खोज करने के लिए के लिए एक तरह से जोड़ना चाहते हैं। "
"इस उदाहरण में मैं आपको बताएंगे कि कैसे आइटम प्रकार से नवीनतम मदों की एक मेनू नीचे खींच बनाने "
"के लिए करेंगे। इन सुझावों के रूप में अच्छी तरह से संग्रह कोड या ठंडे बस्ते में डालने के लिए स्थानों "
"(एक जोड़े परिवर्तन के साथ) के लिए काम करेंगे।"

#: ../../source/extending_koha.rst:77
msgid ""
"First, it's important to note that every link in Koha is a permanent link. "
"This means if I do a search for everything of a specific item type sorted by "
"the acquisitions date and bookmark that URL, whenever I click it I'll see "
"the newest items of that type on the first few pages of the results."
msgstr ""
"सबसे पहले, यह ध्यान दें कि कोहा में हर कड़ी एक स्थायी कड़ी है महत्वपूर्ण है। इसका मतलब यह है "
"कि अगर मैं अधिग्रहण की तारीख से और उस यूआरएल बुकमार्क हल किया एक विशेष आइटम प्रकार का "
"सब कुछ के लिए एक खोज करते हैं, जब भी मैं इसे क्लिक करें मैं परिणामों के पहले कुछ पन्नों पर उस "
"प्रकार के नए आइटम देखेंगे।"

#: ../../source/extending_koha.rst:83
msgid ""
"I took this knowledge and wrote a form takes this functionality in to "
"consideration. It basically just does a search of your Koha catalog for a "
"specific item type and sorts the results by acquisitions date."
msgstr ""
"मैं इस ज्ञान लिया और लिखा एक रूप से विचार करने के लिए इस कार्यक्षमता लेता है। यह मूल रूप "
"से सिर्फ एक विशेष आइटम प्रकार के लिए अपने कोहा सूची का एक खोज करता है और अधिग्रहण की "
"तारीख से परिणाम तरह।"

#: ../../source/extending_koha.rst:87
msgid ""
"The first thing I did was write a MySQL statement to generate a list of item "
"types for me - why copy and paste when you can use the power of MySQL?"
msgstr ""
"पहली बात मैं एक MySQL बयान मेरे लिए आइटम प्रकार की एक सूची उत्पन्न करने के बारे में था - "
"क्यों कॉपी और पेस्ट जब आप MySQL की शक्ति का उपयोग कर सकते हैं?"

#: ../../source/extending_koha.rst:95
msgid ""
"The above looks at the itemtypes table and slaps the necessary HTML around "
"each item type for me. I then exported that to CSV and opened it in my text "
"editor and added the other parts of the form."
msgstr ""
"आइटम प्रकार की मेज पर ऊपर लग रहा है और आवश्यक HTML थप्पड़ मेरे लिए प्रत्येक आइटम के "
"प्रकार के आसपास। मैं तो सीएसवी है कि निर्यात और मेरे पाठ संपादक में इसे खोला और फार्म के "
"अन्य भागों के लिए जोड़ा।"

#: ../../source/extending_koha.rst:119
msgid "Now, what does all of that mean? The important bits are these:"
msgstr "अब, क्या इसका मतलब यह है कि सभी करता है? महत्वपूर्ण बिट्स ये हैं:"

#: ../../source/extending_koha.rst:121
msgid "First the starting of the form."
msgstr "पहले फार्म के शुरुआती।"

#: ../../source/extending_koha.rst:127
msgid ""
"This tells the browser to take any value selected and put it at the end of "
"this http://YOURSITE/cgi-bin/koha/opac-search.pl. If you want to embed this "
"form on your library website (and not on your OPAC) you can put the full "
"OPAC URL in there."
msgstr ""
"यह ब्राउज़र बताता हैं कि चयनित किसी मान ले और इस के अंत में इसे लगाने के लिए http://"
"YOURSITE/cgi-bin/koha/opac-search.pl. आप अपने पुस्तकालय वेबसाइट पर इस फार्म एम्बेड "
"करने के लिए चाहते हैं (और अपने ओपेक पर नहीं) आप पूर्ण ओपेक यूआरएल वहाँ में डाल सकते हैं।"

#: ../../source/extending_koha.rst:132
msgid ""
"Next, there is a hidden value that is telling the search to sort by "
"acquisitions date descending (newest items at the top):"
msgstr ""
"अगले, वहाँ एक छिपा मूल्य है कि अधिग्रहण की तारीख उतरते द्वारा सुलझाने के लिए खोज कह रही "
"है (शीर्ष पर नए आइटम):"

#: ../../source/extending_koha.rst:139
msgid ""
"And finally you have an option for each item type you want people to search."
msgstr ""
"और अंत में आप प्रत्येक आइटम के प्रकार आप लोगों को खोज करना चाहते हैं के लिए एक विकल्प है।"

#: ../../source/extending_koha.rst:146
msgid ""
"These options each include the string \"mc-itype:\" which tells Koha to do "
"an item type search."
msgstr ""
"जो कोहा बताता है एक आइटम प्रकार खोज करते हैं: ये विकल्प प्रत्येक स्ट्रिंग \"mc-itype:\" "
"शामिल हैं।"

#: ../../source/extending_koha.rst:149
msgid ""
"Once you have all of that in place you can copy and paste the form to "
"somewhere on your OPAC. The `Farmington Public Libraries OPAC <http://"
"catalog.farmingtonlibraries.org>`__ has a few examples of this on the left."
msgstr ""
"एक बार जब आप सब कुछ उस जगह पर हो जाएं तो आप फॉर्म को अपनी ओपेक पर कहीं भी कॉपी और "
"पेस्ट कर सकते हैं। 'फार्मिंगटन पब्लिक लाइब्रेरी ओपेक <http://catalog."
"farmingtonlibraries.org>`__ के बाईं ओर इस के कुछ उदाहरण हैं।"

#: ../../source/extending_koha.rst:157
msgid "New titles slider for OPAC"
msgstr "ओपेक के लिए नई शीर्षक स्लाइडर"

#: ../../source/extending_koha.rst:159
msgid ""
"Often times libraries will want to add a flowing widget with new materials "
"at the library to their main public catalog page. To do this you can use a "
"widget from any number of services (usually for a cost) or you can `enable "
"plugins <#pluginsystem>`__ in Koha and use the `Cover Flow plugin <http://"
"git.bywatersolutions.com/koha-plugins.git/shortlog/refs/heads/cover_flow>`__ "
"which is based on the `Flipster <https://github.com/drien/jquery-"
"flipster>`__, a responsive jQuery coverflow plugin."
msgstr ""
"अक्सर पुस्तकालय लाइब्रेरी में अपने मुख्य सार्वजनिक सूची पृष्ठ पर नई सामग्री के साथ बहने वाले "
"विजेट को जोड़ना चाहते हैं। ऐसा करने के लिए आप किसी भी प्रकार की सेवाओं (आमतौर पर लागत "
"के लिए) से विजेट का उपयोग कर सकते हैं या आप कोहा में प्लगइन <#pluginsystem>`__ सक्षम "
"कर सकते हैं और 'कवर फ्लो प्लगइन <http://git.bywatersolutions.com/koha-plugins.git/"
"shortlog/refs/heads/cover_flow>`__ का उपयोग कर सकते हैं। जो 'फ्लिपस्टर <https://"
"github.com/drien/jquery-flipster>`__,एक उत्तरदायी jQuery कवरफ्लो प्लगइन पर "
"आधारित है।"

#: ../../source/extending_koha.rst:169
msgid ""
"Once the plugin is installed, the steps to get your coverflow to show up are "
"as follows:"
msgstr ""
"एक बार प्लगइन स्थापित किया गया है, कदम को दिखाने के लिए इस प्रकार हैं अपने कवरफ्लो पाने "
"के लिए:"

#: ../../source/extending_koha.rst:172
msgid ""
"First, you need to create one or more public reports for your coverflow "
"widget or widgets to be based on. This is how the plugin knows what the "
"content of your widget should contain. Each report needs only three columns; "
"title, biblionumber, and isbn. It is important that you have a good and "
"valid isbn, as that is the datum used to actually fetch the cover. In the "
"iteration of the plugin, we are using Amazon cover images, but I believe in "
"the end I will make the cover image fetcher configurable so we can use any "
"data source for cover image fetching."
msgstr ""
"सबसे पहले, आप अपने कवरफ्लो विजेट या विजेट्स के लिए एक या एक से अधिक सार्वजनिक रिपोर्ट के "
"आधार पर किया जाना है बनाने की जरूरत है। इस प्लगइन कैसे जानता है कि क्या अपने विजेट की "
"सामग्री को शामिल करना चाहिए है। प्रत्येक रिपोर्ट केवल तीन स्तंभों की जरूरत है; शीर्षक, "
"biblionumber, और आईएसबीएन। यह महत्वपूर्ण है कि आप एक अच्छे और वैध आईएसबीएन है, के रूप "
"में है कि वास्तव में कवर लाने के लिए इस्तेमाल किया दत्त है। प्लगइन की यात्रा में, हम अमेज़न "
"कवर छवियों का उपयोग कर रहे हैं, लेकिन मैं अंत में विश्वास मैं कवर छवि fetcher विन्यास कर "
"देगा तो हम कवर छवि fetching के लिए किसी भी डेटा स्रोत का उपयोग कर सकते हैं।"

#: ../../source/extending_koha.rst:181
msgid ""
"Second, we need to configure the plugin. The plugin configuration is a "
"single text area that uses YAML ( actually, it’s JSON, whcih is a subset of "
"YAML ) to store the configuration options. In this example it looks like "
"this:"
msgstr ""
"दूसरा, हम प्लगइन विन्यस्त करने की जरूरत है। प्लगइन विन्यास एकल पाठ क्षेत्र YAML उपयोग "
"करता है कि विन्यास विकल्प स्टोर करने के लिए (वास्तव में, यह JSON, whcih YAML के एक "
"सबसेट है)। इस उदाहरण में यह इस तरह दिखता है:"

#: ../../source/extending_koha.rst:193
msgid ""
"In this example, we are telling the plugin to use the report with id 42, and "
"use it to create a coverflow widget to replace the HTML element with the id "
"“coverflow”. The options list is passed directly to Flipster, so any options "
"supported by Flipster can be set from the plugin configuration! In fact, in "
"addition to the traditional coverflow, Flipster has a “carousel” mode which "
"is a much more compact version of the coverflow. You can also configure "
"which cover the widget will start on, among other options."
msgstr ""
"इस उदाहरण में, हम आईडी 42 के साथ रिपोर्ट का उपयोग करने के लिए प्लगइन कह रहे हैं, और "
"इसका इस्तेमाल आईडी \"कवरफ्लो\" के साथ HTML तत्व को बदलने के लिए एक कवरफ्लो विजेट बनाने "
"के लिए। विकल्पों की सूची सीधे Flipster को पारित कर दिया है, तो Flipster द्वारा "
"समर्थित किसी भी विकल्प प्लगइन विन्यास से सेट किया जा सकता है! असल में, पारंपरिक कवरफ्लो "
"के अलावा, Flipster एक \"हिंडोला\" मोड कवरफ्लो की एक और अधिक कॉम्पैक्ट संस्करण है जो "
"है। तुम भी जो कवर विजेट पर शुरू होगा विन्यस्त कर सकते हैं, अन्य विकल्पों के बीच।"

#: ../../source/extending_koha.rst:202
msgid ""
"At the time the plugins options are saved or updated, the plugin will then "
"generate some minified JavaScript code that is automatically stored in the "
"Koha system preference OPACUserJS. Here is an example of the output:"
msgstr ""
"समय plugins विकल्प बचाया या अद्यतन कर रहे हैं पर, प्लगइन तो यह है कि स्वचालित रूप से "
"कोहा प्रणाली वरीयता OPACUserJS में संग्रहित है कुछ minified जावास्क्रिप्ट कोड उत्पन्न "
"होगा। यहाँ आउटपुट का एक उदाहरण है:"

#: ../../source/extending_koha.rst:214
msgid ""
"Why do this? For speed! Rather than regenerating this code each and every "
"time the page loads, we can generate it once, and use it over and over again."
msgstr ""
"यह क्यों? गति के लिए! बल्कि इस कोड को प्रत्येक और हर बार पेज लोड पैदा करने के बजाय, हम "
"इसे एक बार उत्पन्न करके, और फिर इसका इस्तेमाल कर सकते हैं।"

#: ../../source/extending_koha.rst:218
msgid ""
"If you inspect the code closely, you’ll notice it references a script "
"“coverflow.pl”. This is a script that is included with the coverflow plugin. "
"Since we need to access this from the OPAC ( and we don’t want to set off "
"any XSS attack alarms ), we need to modify the web server configuration for "
"the public catalog and add the followup to it:"
msgstr ""
"आप निकट कोड का निरीक्षण किया, तो आप इसे एक स्क्रिप्ट \"coverflow.pl\" संदर्भ नोटिस "
"देंगे। यह एक स्क्रिप्ट है कि कवरफ्लो प्लगइन के साथ शामिल है। चूंकि हम ओपेक से इस का उपयोग "
"करने की जरूरत है (और हम किसी भी XSS हमले अलार्म बंद सेट नहीं करना चाहते हैं), हम जनता "
"सूची के लिए वेब सर्वर विन्यास को संशोधित करने और इसे करने के लिए फॉलोअप जोड़ने की जरूरत है:"

#: ../../source/extending_koha.rst:228
msgid ""
"This line gives us access to the coverflow.pl script from the OPAC. This "
"script retrieves the report data and passes it back to the public catalog "
"for creating the coverflow widget. Koha::Cache is supported in order to make "
"the widget load as quickly as possible!"
msgstr ""
"इस लाइन हमें ओपेक से coverflow.pl स्क्रिप्ट के लिए पहुँच देता है। यह स्क्रिप्ट रिपोर्ट डेटा "
"प्राप्त करता है और कवरफ्लो विजेट बनाने के लिए इसे वापस सार्वजनिक सूची से गुजरता है। Koha::"
"Cache जितनी जल्दी संभव हो विजेट लोड बनाने के लिए क्रम में समर्थन किया है!"

#: ../../source/extending_koha.rst:233
msgid ""
"The final step is to put your selector element somewhere in your public "
"catalog. In this example, I put the following in the system preference "
"OpacMainUserBlock:"
msgstr ""
"अंतिम चरण के लिए अपने सार्वजनिक सूची में कहीं न कहीं अपने चयनकर्ता तत्व डाल दिया है। इस "
"उदाहरण में, मैं प्रणाली वरीयता OpacMainUserBlock में निम्नलिखित डाले:"

#: ../../source/extending_koha.rst:241
msgid ""
"Once that is in place, you need only refresh your OPAC page, and there you "
"have it, your very own catalog coverflow widget! Not only do these "
"coverflows look great on a computer screen, but they look great on mobile "
"platforms as well, and are even touch responsive!"
msgstr ""
"एक बार उस जगह में है, तो आप अपने ओपेक पृष्ठ ताज़ा केवल जरूरत है, और वहाँ तुम्हारे पास है, "
"अपने बहुत ही सूची कवरफ्लो विजेट! इतना ही नहीं इन coverflows एक कंप्यूटर स्क्रीन पर बहुत "
"अच्छी लग रही हो, लेकिन वे भी मोबाइल प्लेटफॉर्म पर बहुत अच्छी लग रही है, और संवेदनशील "
"स्पर्श भी कर रहे हैं!"

#: ../../source/extending_koha.rst:246
msgid "|image1316|"
msgstr "|image1316|"

#: ../../source/extending_koha.rst:251
msgid "Cataloging and Searching by Color"
msgstr "रंग द्वारा सूचीकरण और खोज"

#: ../../source/extending_koha.rst:253
msgid ""
"One of the icon sets installed in Koha includes a series of colors. This set "
"can be used to catalog and search by color if you'd like. This guide will "
"walk you use changing collection code to color in Koha so that you can do "
"this."
msgstr ""
"चिह्न सेट कोहा में स्थापित की एक रंग की एक श्रृंखला शामिल है। इस सेट सूची और रंग के द्वारा "
"खोज यदि आप चाहते हैं के लिए इस्तेमाल किया जा सकता हैं. इस गाइड चलना होगा आप संग्रह कोड "
"को बदलने का उपयोग कोहा में रंग करने के लिए इतना है कि आप यह कर सकते हैं।"

#: ../../source/extending_koha.rst:258
msgid ""
"The following SQL could be used to add these colors to the CCODE authorized "
"value category in a batch. If you wanted to use these colors for another "
"authorized value you'd have to edit this to use that category:"
msgstr ""
"निम्नलिखित एसक्यूएल एक बैच में CCODE अधिकृत मूल्य वर्ग के लिए इन रंगों को जोड़ने के लिए "
"इस्तेमाल किया जा सकता है। यदि आप किसी अन्य अधिकृत मूल्य के लिए इन रंगों का उपयोग करना "
"चाहता है, तो आप इस संपादित करने के लिए उस श्रेणी का इस्तेमाल करना चाहते हैं:"

#: ../../source/extending_koha.rst:310
msgid ""
"If you would like to choose the colors manually you can do that via the :ref:"
"`Authorized Values` administration area."
msgstr ""
"यदि आप मैन्युअल रूप से रंग चुनना चाहते हैं तो आप इसे के माध्यम से कर सकते हैं :ref:`अधिकृत मान "
"'प्रशासन क्षेत्र।"

#: ../../source/extending_koha.rst:313
msgid "|image1118|"
msgstr "|image1118|"

#: ../../source/extending_koha.rst:315
msgid ""
"Next you'll want to :ref:`update the frameworks <marc-bibliographic-"
"frameworks-label>` so that the 952$8 (if you're using collection code) label "
"to says Color."
msgstr ""
"इसके बाद आप चाहते हैं :ref:`फ्रेमवर्क को अपडेट करें <marc-bibliographic-frameworks-"
"label>' ताकि रंग कहने के लिए 952$8 (यदि आप संग्रह कोड का उपयोग कर रहे हों) लेबल।"

#: ../../source/extending_koha.rst:318
msgid "Once you have that in place you can start to catalog items by color."
msgstr "एक बार जब आप उस जगह में आप रंग द्वारा आइटम सूची को शुरू कर सकते हैं।"

#: ../../source/extending_koha.rst:320
msgid ""
"Finally you'll want to add the following JQuery to your preferences so that "
"it will relabel 'Collection' to 'Color'"
msgstr ""
"अंत में आप अपनी प्राथमिकताओं के निम्नलिखित jQuery जोड़ने के लिए चाहते हैं इतना है कि यह "
"'रंग' के लिए 'संग्रह' relabel जाएगा"

#: ../../source/extending_koha.rst:323
msgid ":ref:`IntranetUserJS`"
msgstr ":ref:`IntranetUserJS`"

#: ../../source/extending_koha.rst:332
msgid ":ref:`OPACUserJS`"
msgstr ":ref:`OPACUserJS`"

#: ../../source/extending_koha.rst:345
msgid "Using Koha as a Content Management System (CMS)"
msgstr "कोहा एक सामग्री प्रबंधन प्रणाली के रूप में उपयोग करना (सीएमएस)"

#: ../../source/extending_koha.rst:350
msgid "Setup"
msgstr "सेटअप"

#: ../../source/extending_koha.rst:352
msgid ""
"These are instructions for taking a default install of Koha and allowing it "
"to function as a little content management system. This will allow a library "
"to publish an arbitrary number of pages based on a template. This example "
"uses the template for the main opac page, but you could just as well use any "
"template you wish with a bit more editing. This may be appropriate if you "
"have a small library, want to allow librarians to easily add pages, and do "
"not want to support a complete CMS."
msgstr ""
"ये एक डिफ़ॉल्ट लेने के लिए निर्देश हैं कोहा के स्थापित और यह एक छोटे सामग्री प्रबंधन प्रणाली "
"के रूप में कार्य करने की अनुमति है। यह एक पुस्तकालय टेम्पलेट पर आधारित पृष्ठों की एक मनमाना "
"संख्या को प्रकाशित करने की अनुमति देगा। यह उदाहरण मुख्य ओपेक पेज के लिए टेम्पलेट का उपयोग "
"करता है, लेकिन आप बस के रूप में अच्छी तरह से किसी भी टेम्पलेट आप थोड़ा और अधिक संपादन के "
"साथ इच्छा इस्तेमाल कर सकते हैं। यदि आप एक छोटे पुस्तकालय है यह उपयुक्त हो सकता है, "
"पुस्तकालयाध्यक्षों आसानी से पृष्ठों को जोड़ने के लिए अनुमति देना चाहते हैं, और एक पूरा सीएमएस "
"समर्थन करने के लिए नहीं करना चाहती."

#: ../../source/extending_koha.rst:360
msgid ""
"Copy /usr/share/koha/opac/cgi-bin/opac/opac-main.pl to /usr/share/koha/opac/"
"cgi-bin/opac/pages.pl (in the same directory)"
msgstr ""
"कॉपी /usr/share/koha/opac/cgi-bin/opac/opac-main.pl to /usr/share/koha/opac/"
"cgi-bin/opac/pages.pl (एक ही निर्देशिका में)"

#: ../../source/extending_koha.rst:363
msgid "Edit pages.pl in an editor"
msgstr "एक संपादक में pages.pl संपादित करें"

#: ../../source/extending_koha.rst:365
msgid "At approximately line 33 change this code:"
msgstr "लगभग 33 लाइन पर बदलें इस कोड को:"

#: ../../source/extending_koha.rst:371
msgid "To this code:"
msgstr "इस कोड के लिए:"

#: ../../source/extending_koha.rst:377
msgid "At approximately line 62 after this code:"
msgstr "लगभग 62 लाइन इस कोड के बाद:"

#: ../../source/extending_koha.rst:388
msgid "Add these lines:"
msgstr "इन लाइनों को जोड़ें:"

#: ../../source/extending_koha.rst:396
msgid ""
"Note pages.pl file must have Webserver user execution permissions, you can "
"use `chmod <http://en.wikipedia.org/wiki/Chmod>`__ command if you are "
"actually logged in as such user:"
msgstr ""
"नोट पेज.पी फ़ाइल में वेबसर्वर उपयोगकर्ता निष्पादन अनुमतियां होनी चाहिए, यदि आप वास्तव में "
"ऐसे उपयोगकर्ता के रूप में लॉग इन हैं तो आप `chmod <http://en.wikipedia.org/wiki/"
"Chmod>` __ कमांड का उपयोग कर सकते हैं:"

#: ../../source/extending_koha.rst:404
msgid ""
"In the browser go to Home > Administration > System Preferences > Local Use "
"and add a New Preference called \"page\\_test\""
msgstr ""
"ब्राउज़र में होम> व्यवस्थापन> सिस्टम प्राथमिकताएं> स्थानीय उपयोग पर जाएं और एक नई "
"प्राथमिकता जोड़ें \"page\\_test\""

#: ../../source/extending_koha.rst:407
msgid "Fill it out as so"
msgstr "के रूप में तो यह भरें"

#: ../../source/extending_koha.rst:409
msgid "Explanation: test page for pages tiny cms"
msgstr "स्पष्टीकरण: पृष्ठों टिनी सीएमएस के लिए परीक्षण पृष्ठ"

#: ../../source/extending_koha.rst:411
msgid "Variable: page\\_test"
msgstr "परिवर्तनशील: page\\_test"

#: ../../source/extending_koha.rst:413
msgid "Value: Lorem ipsum"
msgstr "मान: लोरेम इपसम"

#: ../../source/extending_koha.rst:415
msgid ""
"Click the TextArea link (or enter \"TextArea\" into the input field below it)"
msgstr "TextArea लिंक पर क्लिक करें (या इसके नीचे इनपुट क्षेत्र में \"TextArea\" में प्रवेश)"

#: ../../source/extending_koha.rst:418
msgid "variable options (last field): 80\\|50"
msgstr "चर विकल्प (आखिरी क्षेत्र): 80\\|50"

#: ../../source/extending_koha.rst:420
msgid ""
"In a browser go to http://youraddress/cgi-bin/koha/pages.pl?p=test The page "
"should come up with the words \"Lorem ipsum\" in the main content area of "
"the page. (replace \"youraddress\" with localhost, 127.0.0.1, or your domain "
"name depending on how you have Apache set up.)"
msgstr ""
"एक ब्राउज़र में करने के लिए जाना http://youraddress/cgi-bin/koha/pages.pl?p=test "
"पेज शब्द \"Lorem Ipsum\" पेज के मुख्य सामग्री क्षेत्र में साथ आना चाहिए. (स्थानीय होस्ट के "
"साथ \"अपनापता\" की जगह, 127.0.0.1, या अपने डोमेन नाम कैसे आप अपाचे की स्थापना की है "
"पर निर्भर करता है.)"

#: ../../source/extending_koha.rst:426
msgid ""
"To add more pages simply create a system preference where the title begins "
"with \"page\\_\" followed by any arbitrary letters. You can add any markup "
"you want as the value of the field. Reference the new page by changing the "
"value of the \"p\" parameter in the URL."
msgstr ""
"अधिक पेज जोड़ने के लिए बस एक सिस्टम प्राथमिकता बनाएं जहां शीर्षक  \"page\\_\" के साथ "
"शुरू होता है जिसके बाद किसी भी मनमाना अक्षर होते हैं। आप फ़ील्ड के मान के रूप में इच्छित "
"मार्कअप जोड़ सकते हैं। यूआरएल में \"पी\" पैरामीटर के मान को बदलकर नए पेज का संदर्भ लें।"

#: ../../source/extending_koha.rst:431
msgid ""
"To learn more visit the Koha wiki page on this topic: http://wiki.koha-"
"community.org/wiki/Koha_as_a_CMS"
msgstr ""
"इस विषय पर अधिक जानने के लिए कोहा विकि पृष्ठ पर जाएँ : http://wiki.koha-community."
"org/wiki/Koha_as_a_CMS"

#: ../../source/extending_koha.rst:437
msgid "Editing the pages template"
msgstr "पेज टेम्पलेट को संपादित करना"

#: ../../source/extending_koha.rst:439
msgid ""
"Copy /usr/share/koha/opac/htdocs/opac-tmpl/bootstrap/en/modules/opac-main.tt "
"to /usr/share/koha/opac/htdocs/opac-tmpl/bootstrap/en/modules/pages.tt"
msgstr ""
"कॉपी /usr/share/koha/opac/htdocs/opac-tmpl/bootstrap/en/modules/opac-main.tt "
"से /usr/share/koha/opac/htdocs/opac-tmpl/bootstrap/en/modules/pages.tt"

#: ../../source/extending_koha.rst:444
msgid ""
"Edit /usr/share/koha/opac/htdocs/opac-tmpl/bootstrap/en/modules/pages.tt"
msgstr ""
"संपादित /usr/share/koha/opac/htdocs/opac-tmpl/bootstrap/en/modules/pages.tt"

#: ../../source/extending_koha.rst:447
msgid "At approximately line 61, change this:"
msgstr "लगभग लाइन 61 पर, यह बदलने के लिए:"

#: ../../source/extending_koha.rst:453
msgid "To this:"
msgstr "इस के लिए:"

#: ../../source/extending_koha.rst:459
msgid ""
"Remark: You may wish to disable your News block of these CMS style pages e."
"g. when you do not want it displayed on the CMS style pages or where the "
"News block is long enough that it actually makes the 'page\\_test' include "
"scroll outside the default viewport dimensions. In that case, remove the "
"following code from your pages.tt template."
msgstr ""
"टिप्पणी: आप इन सीएमएस शैली पृष्ठों के अपने समाचार ब्लॉक को अक्षम करना चाहते हैं उदा। जब "
"आप नहीं चाहते कि यह सीएमएस स्टाइल पेजों पर प्रदर्शित हो या जहां न्यूज़ ब्लॉक काफी लंबा "
"हो, तो यह वास्तव में'page\\_test' को डिफ़ॉल्ट व्यूपोर्ट आयामों के बाहर स्क्रॉल भी शामिल "
"करता है। उस स्थिति में, अपने pages.tt से निम्नलिखित कोड हटाएं। टेम्पलेट।"

#: ../../source/extending_koha.rst:483
msgid "Troubleshooting"
msgstr "समस्या निवारण"

#: ../../source/extending_koha.rst:485
msgid ""
"If you have problems check file permissions on pages.pl and pages.tt. They "
"should have the same user and group as other Koha files like opac-main.pl."
msgstr ""
"यदि आपको समस्याएं हैं। Pages.pl और pages.tt पर फ़ाइल अनुमतियां जांचें। उनके पास opac-"
"main.pl जैसे अन्य कोहा फ़ाइलों के समान उपयोगकर्ता और समूह होना चाहिए।"

#: ../../source/extending_koha.rst:492
msgid "Bonus Points"
msgstr "बोनस अंक"

#: ../../source/extending_koha.rst:494
msgid ""
"Instead of using the address http://youraddress/cgi-bin/koha/pages.pl?p=test "
"you can shorten it to http://youraddress/pages.pl?p=test Just open up /etc/"
"koha/koha-httpd.conf and add the follow at about line 13:"
msgstr ""
"पते का उपयोग करने के बजाय http://youraddress/cgi-bin/koha/pages.pl?p=test आप इसे "
"करने छोटा कर सकते हैं http://youraddress/pages.pl?p=test केवल ऊपर खोले /etc/koha/"
"koha-httpd.conf और लगभग लाइन 13 पर अनुसरण कर जोड़े:"

#: ../../source/extending_koha.rst:503
msgid "Then restart Apache."
msgstr "फिर अपाचे पुनः आरंभ."

#: ../../source/extending_koha.rst:508
msgid "Usage"
msgstr "उपयोग"

#: ../../source/extending_koha.rst:510
msgid ""
"After setting up Koha as a CMS you can create new pages following these "
"instructions:"
msgstr ""
"एक सीएमएस के रूप में कोहा की स्थापना के बाद आप इन निर्देशों का पालन नया पेज बना सकते हैं:"

#: ../../source/extending_koha.rst:516
msgid "Adding Pages"
msgstr "पेज जोड़ना"

#: ../../source/extending_koha.rst:518
msgid "To add a new page you need to add a system preference under Local Use."
msgstr ""
"एक नया पृष्ठ जोड़ने के लिए आप स्थानीय उपयोग के तहत एक सिस्टम प्राथमिकता जोड़ने की जरूरत "
"हैं."

#: ../../source/extending_koha.rst:520
msgid ""
"Get there: More > Administration > Global System Preferences > Local Use"
msgstr "वहाँ जाओ: अधिक >प्रशासन > ग्लोबल सिस्टम प्राथमिकताएं > स्थानीय उपयोग"

#: ../../source/extending_koha.rst:523
msgid "Click 'New Preference'"
msgstr "क्लिक करें 'नई वरीयता'"

#: ../../source/extending_koha.rst:525
msgid "Enter in a description in the Explanation field"
msgstr "स्पष्टीकरण क्षेत्र में एक विवरण में दर्ज करें"

#: ../../source/extending_koha.rst:527
msgid "Enter a value that starts with 'page\\_' in the Variable field"
msgstr "वैरिएबल फ़ील्ड में  'page\\_' से शुरू होने वाला मान दर्ज करें"

#: ../../source/extending_koha.rst:529
msgid "Enter starting HTML in the Value field"
msgstr "मूल्य क्षेत्र में एचटीएमएल शुरू होने वाले दर्ज करें"

#: ../../source/extending_koha.rst:531
msgid "|image1076|"
msgstr "|image1076|"

#: ../../source/extending_koha.rst:533
msgid "Set the Variable Type to Textarea"
msgstr "Textarea को वेरिएबल प्रकार सेट करें"

#: ../../source/extending_koha.rst:535
msgid ""
"Set the Variable options to something like 20\\|20 for 20 rows and 20 columns"
msgstr "परिवर्तनीय विकल्प 20\\|20 के लिए 20 पंक्तियों और 20 कॉलम कुछ सेट करें"

#: ../../source/extending_koha.rst:538
msgid "|image1077|"
msgstr "|image1077|"

#: ../../source/extending_koha.rst:543
msgid "Viewing your page"
msgstr "अपना पेज देखना"

#: ../../source/extending_koha.rst:545
msgid ""
"You can view your new page at http://YOUR-OPAC/cgi-bin/koha/pages.pl?"
"p=PAGENAME where PAGENAME is the part you entered after 'page\\_' in the "
"Variable field."
msgstr ""
"आप अपना नया पृष्ठ http://YOUR-OPAC/cgi-bin/koha/pages.pl?p=PAGENAME पर देख सकते "
"हैंE जहां PAGENAME वह हिस्सा है जिसे आपने वेरिएबल फ़ील्ड में 'page\\_' के बाद दर्ज किया है।"

#: ../../source/extending_koha.rst:551
#, fuzzy
msgid "**Example**"
msgstr "उदाहरण"

#: ../../source/extending_koha.rst:553
msgid ""
"This process can be used to create recommended reading lists within Koha. So "
"once the code changes have been made per the instructions on 'Koha as a CMS' "
"you go through the 'Adding a New Page' instructions above to great a page "
"for 'Recommended Reading Lists'"
msgstr ""
"इस प्रक्रिया कोहा के भीतर की सिफारिश की पढ़ने की सूची बनाने के लिए इस्तेमाल किया जा "
"सकता है। इसलिए एक बार कोड में परिवर्तन 'कोहा एक सीएमएस' के रूप में' पर दिए गए निर्देशों "
"के अनुसार बनाया गया है आप के ऊपर 'जोड़ना एक नया पृष्ठ' के निर्देशों के माध्यम से जाएगें "
"'अनुशंसित पढ़ने की सूची' के लिए महान एक पृष्ठ पर"

#: ../../source/extending_koha.rst:558
msgid "|image1078|"
msgstr "|image1078|"

#: ../../source/extending_koha.rst:560
msgid ""
"Next we need to create pages for our various classes (or categories). To do "
"this, return to the 'Adding a New Page' section and create a preference for "
"the first class."
msgstr ""
"अगले हम हमारे विभिन्न वर्गों (या श्रेणियों) के लिए पेज बनाने के लिए की जरूरत है। ऐसा करने के "
"लिए, 'जोड़ने के लिए एक नया पृष्ठ' अनुभाग में लौटने और प्रथम श्रेणी के लिए एक प्राथमिकता "
"बनाए."

#: ../../source/extending_koha.rst:564
msgid "|image1079|"
msgstr "|image1079|"

#: ../../source/extending_koha.rst:566
msgid ""
"Next you'll want to link your first page to your new second page, go to the "
"page\\_recommend preference and click 'Edit.' Now you want to edit the HTML "
"to include a link to your newest page:"
msgstr ""
"इसके बाद आप अपने पहले पृष्ठ को अपने नए दूसरे पृष्ठ से लिंक करना चाहेंगे, page\\_recommend "
"पर जाएं और 'संपादित करें' पर क्लिक करें। अब आप एचटीएमएल को अपने नवीनतम पेज के लिंक को "
"शामिल करने के लिए संपादित करना चाहते हैं:"

#: ../../source/extending_koha.rst:570
msgid "|image1080|"
msgstr "|image1080|"

#: ../../source/extending_koha.rst:574
#, fuzzy
msgid "**Live Examples**"
msgstr "लाइव उदाहरण"

#: ../../source/extending_koha.rst:576
msgid ""
"The Crawford Library at Dallas Christian College is using this method for "
"their recommended reading lists: http://opac.dallas.edu/"
msgstr ""
"डलास क्रिश्चियन कॉलेज में क्रॉफर्ड लाइब्रेरी इस विधि का उपयोग उनकी अनुशंसित पढ़ने सूचियों के "
"लिए कर रही है: http://opac.dallas.edu/"

#: ../../source/extending_koha.rst:580
msgid "Koha search on your site"
msgstr "आपकी साइट पर कोहा खोज"

#: ../../source/extending_koha.rst:582
msgid ""
"Often you'll want to add a Koha search box to your library website. To do "
"so, just copy and paste the following code in to your library website and "
"update the YOURCATALOG bit with your catalog's URL and you're set to go."
msgstr ""
"अक्सर आप अपने पुस्तकालय वेबसाइट के लिए एक कोहा खोज बॉक्स में जोड़ने के लिए चाहता हूँ। ऐसा "
"करने के लिए, बस कॉपी और अपने पुस्तकालय वेबसाइट में निम्नलिखित कोड पेस्ट करें और अपनी सूची "
"के URL के साथ YOURCATALOG बिट अद्यतन और तुम जाने के लिए तैयार हैं।"
